﻿using MercadoPago.Common;
using MercadoPago.DataStructures.Preference;
using MercadoPago.Resources;
using MercadoPagoWebCheckoutNetCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;

namespace MercadoPagoWebCheckoutNetCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration config;

        public MercadoPagoSetting mpSetting { get; private set; }

        public HomeController(IConfiguration config)
        {
            var emailOptions = config.GetSection("MercadoPagoSettings").Get<MercadoPagoSetting>();

            mpSetting = emailOptions;
        }


        public IActionResult Index(string cardToken, string email)
        {
            if (String.IsNullOrWhiteSpace(cardToken) || string.IsNullOrWhiteSpace(email))
                return BadRequest();

            if (String.IsNullOrWhiteSpace(MercadoPago.SDK.AccessToken))
            {
                MercadoPago.SDK.AccessToken = GetAccessToken();
            }

            var productId = Guid.NewGuid().ToString();
            
            
            var urlMPApi = GetPreferences(productId, email, cardToken);

            ViewData["data"] = urlMPApi;

            return View();
        }

        [NonAction]
        private string GetPreferences(string productId, string email, string cardToken)
        {
            var baseUrl = mpSetting.Redirection.BaseUrl;

            mpSetting.Redirection.Fail = mpSetting.Redirection.Fail
                .Replace("{url}", baseUrl).Replace("{id}", productId);

            mpSetting.Redirection.Pending = mpSetting.Redirection
                .Pending.Replace("{url}", baseUrl).Replace("{id}", productId);

            mpSetting.Redirection.Success = mpSetting.Redirection.Success
                .Replace("{url}", baseUrl).Replace("{id}", productId);


          

            // Crea un objeto de preferencia
            var preference = new Preference
            {
                Items = {
                    new Item
                    {
                        Id = productId,
                        Title = "Entrada Teatro",
                        Quantity = 1,
                        CurrencyId = CurrencyId.ARS,
                        UnitPrice = (decimal)105,
                    }
                }
            };


            preference.Save();

            return preference.InitPoint;

        }

        [NonAction]
        private string GetAccessToken()
        {
            return mpSetting.SandBoxMode
                ? mpSetting.Tokens.SandBox
                : mpSetting.Tokens.Production;
        }


        public IActionResult Gateway(string id, string response)
        {
            return View();
        }

        public IActionResult CallBack(MAPIREsponse aPIREsponse)
        {
            return View(aPIREsponse);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
