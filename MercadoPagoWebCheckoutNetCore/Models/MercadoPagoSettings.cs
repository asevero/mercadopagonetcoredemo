﻿namespace MercadoPagoWebCheckoutNetCore.Models
{

    public class MercadoPagoSetting
    {
        public bool SandBoxMode { get; set; }
        public Tokens Tokens { get; set; }
        public Redirection Redirection { get; set; }
        public Endpoint Endpoint { get; set; }
    }

    public class Tokens
    {
        public string Production { get; set; }
        public string SandBox { get; set; }
    }

    public class Redirection
    {
        public string BaseUrl { get; set; }
        public string Success { get; set; }
        public string Fail { get; set; }
        public string Pending { get; set; }
    }

    public class Endpoint
    {
        public string CallBackUrlPaymentAPI { get; set; }
    }

  

}
